<?php

namespace Database\Seeders;

use DB;
use Illuminate\Database\Seeder;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('products')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        \App\Models\Products::create(['name' => 'Fjallraven - Foldsack No. 1 Backpack, Fits 15 Laptops', 'available_stock' => 5]);
        \App\Models\Products::create(['name' => 'Mens Casual Premium Slim Fit T-Shirts', 'available_stock' => 5]);
        \App\Models\Products::create(['name' => 'Mens Cotton Jacket', 'available_stock' => 5]);
        \App\Models\Products::create(['name' => 'Solid Gold Petite Microwave', 'available_stock' => 5]);
        \App\Models\Products::create(['name' => 'WD 2TB Elements Portable External Hard Drive - USB 3.0', 'available_stock' => 5]);
    }
}
