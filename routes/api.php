<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\AuthApiController;
use App\Http\Controllers\API\OrderAPIController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->post('/user', function (Request $request) {
    return $request->user();
});

Route::middleware(['auth:sanctum'])->group(function () {

    Route::controller(OrderAPIController::class)->group(function () {
        Route::post('/orderapi', 'orderapi');
    });

});

Route::controller(AuthApiController::class)->group(function () {
    Route::post('/registerapi', 'registerapi');
    Route::post('/loginapi', 'loginapi');
    Route::post('/logoutapi', 'logoutapi');
});


