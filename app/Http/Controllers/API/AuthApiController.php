<?php

namespace App\Http\Controllers\API;

use DB;
use Auth;
use Session;
use Exception;

use App\Http\Controllers\API\BaseController as BaseController;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AuthApiController extends BaseController
{
    function registerapi(Request $request)
    {

        $check = Validator::make($request->all(), [
            'email' => 'required|email|unique:users',
            'name' => 'required',
            'password' => 'required',
            // 'confirm_password' => ['required', 'same:password'],
        ]);

        if ($check->fails()) {
            return response()->json(['status' => 422, 'error' => $check->messages()->first()]);
        } else {

            try {

                // Begin a transaction
                DB::beginTransaction();

                User::create(request(['name', 'email', 'password']));

                DB::commit();

                return response()->json([
                    'status' => 200,
                    'message' => 'User successfully registered.',

                ]);

            } catch (Exception $e) {
                // An error occured; cancel the transaction...
                DB::rollback();

                // and throw the error again.
                return response()->json(['status' => 500, 'error' => $e->getMessage()]);

            }
        }





    }

    function loginapi(Request $request)
    {
        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){
            $user = Auth::user();
            if(!$user->access_token){
                $token =  $request->user()->createToken('token')->plainTextToken;
               try {
                   DB::beginTransaction();
                   Auth::user()->access_token = $token;
                   Auth::user()->save();
                   DB::commit();
               }catch(Exception $e){
                   DB::rollback();
               }
               $success['hastoken'] =  $user->access_token;
           } else {
               $success['hastoken'] =  $user->access_token;
           }
            $success['userdata'] =  $user;
            auth()->login($user);
            return $this->sendResponse($success, 'User login successfully.');
        }
        else{
            return $this->sendError('Invalid credentials.', ['error'=>'Unauthorised']);
        }
    }

    function logoutapi()
    {


         Auth::logout();

        return Session::flush();
    }

}
