<?php

namespace App\Http\Controllers\API;

use DB;
use Auth;
use Exception;

use App\Models\Products;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class OrderAPIController extends Controller
{
    function orderapi(Request $request){


        if(Auth::check()){
            if($request->id==null){
                return response()->json(['status'=>404,'message'=>"Item ID is missing."]);
            }else{
                $getItem=Products::find($request->id, ['available_stock']);
                if( $getItem==null){

                    return response()->json(['status'=>404,'message'=>"Item not found."]);
                }else{
                    $item_stock = $getItem->available_stock;
                    $total_qty = $item_stock-$request->qty;
                }

            }

            if($request->qty==0 || !$request->qty){
                return response()->json(['status'=>404,'message'=>"Item quantity must not be empty."]);
            }
            if($item_stock!=0 &&  $total_qty>0){
                try{
                    DB::beginTransaction();
                    Products::find($request->id)->update(['available_stock' => $total_qty]);
                    DB::commit();
                    return response()->json(['status'=>200,'message'=>"You have succesfully ordered this product."]);
                }catch(Exception $e){
                    DB::rollback();
                }
            }else{
                return response()->json(['status'=>404,'message'=>"Failed to order this product due to unavailability of the stock."]);
            }

        }else{
            return "Not Authenticated";
        }
    }
}
